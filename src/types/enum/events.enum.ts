export enum EventType {
  GET_CHAT_RES = 'GET_CHAT_RES',
  DELETE_MESSAGE_REQ = 'DELETE_MESSAGE_REQ',
  DELETE_MESSAGE_RES = 'DELETE_MESSAGE_RES',
  NEW_MESSAGE = 'NEW_MESSAGE',

}

export interface EventMessage<T> {
  data?: T;
  traceData: any
}
export interface Message {
  id: string;
  sentBy: string;
  text: string;
  isDeleted: boolean;
  createdAt: Date
}
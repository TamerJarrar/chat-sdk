export interface ChatAppSdkOptions {
  token: string;
  wsUrl: string;
  username: string
}

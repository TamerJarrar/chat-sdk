import { Status } from '../enum/status.enum';

export interface ServiceResponse<T> {
  success: boolean;
  data?: T;
  status: Status;
  message?: string[];
}

import axios, { AxiosResponse } from "axios";
import { ServiceResponse } from '../interface/service-response.interface';
import { ChatAppSdkOptions } from '../interface/chat-app-sdk-options.interface';
import { handleAxiosErrors } from './handle-axios.helper';
import { Status } from '../enum/status.enum';

/**
 * Class representing an authentication service.
 */
export class AuthService {
  /**
   * The base URL of the authentication service.
   * @type {string}
   * @private
   */
  private baseUrl: string = '';

  /**
   * Creates an instance of AuthService.
   * @param {string} baseUrl - The base URL of the authentication service.
   */
  constructor(baseUrl: string) {
    this.baseUrl = baseUrl;
  }

  /**
   * Logs in a user.
   * @param {Object} params - The login parameters.
   * @param {string} params.username - The username of the user.
   * @param {string} params.password - The password of the user.
   * @returns {Promise<ServiceResponse<ChatAppSdkOptions>>} A promise that resolves to the authentication response.
   */
  async login(params: { username: string; password: string }): Promise<ServiceResponse<ChatAppSdkOptions>> {
    try {
      const data = await axios.post<ServiceResponse<ChatAppSdkOptions>>(`${this.baseUrl}/login`, params, {
        headers: {
          Accept: "application/json"
        }
      });
      return data.data;
    } catch (error: any) {
      console.log("Error in login ");
      console.log(error)
      return handleAxiosErrors(error);
    }
  }

  /**
   * Registers a new user.
   * @param {Object} params - The registration parameters.
   * @param {string} params.username - The username of the new user.
   * @param {string} params.password - The password of the new user.
   * @param {string} params.confirmPassword - The confirmation password of the new user.
   * @returns {Promise<ServiceResponse<ChatAppSdkOptions>>} A promise that resolves to the registration response.
   */
  async register(params: { username: string; password: string; confirmPassword: string }): Promise<ServiceResponse<ChatAppSdkOptions>> {
    const response: ServiceResponse<ChatAppSdkOptions> = {
      status: Status.INTERNAL_ERROR,
      success: false
    };
    try {
      if (params.password !== params.confirmPassword) {
        response.status = Status.PASSWORD_AND_PASSWORD_CONFIRMATION_DOSE_NOT_MATCH;
        return response;
      }
      if (params.password.length < 5) {
        response.status = Status.PASSWORD_IS_TOO_SHORT;
        return response;
      }
      const data = await axios.post<ServiceResponse<ChatAppSdkOptions>>(`${this.baseUrl}/register`, params, {
        headers: {
          Accept: "application/json"
        }
      });
      return data.data;
    } catch (error: any) {
      console.log("Error in register ");
      return handleAxiosErrors(error);
    }
  }
}

import { AxiosError } from 'axios';
import { Status } from '../enum/status.enum';
import { ServiceResponse } from '../interface/service-response.interface';

export function handleAxiosErrors(err: AxiosError<unknown>) {
  const res: ServiceResponse<any> = {
    success: false,
    status: Status.INTERNAL_ERROR,
  };
  if (err.response) {
    // Handle specific error status codes
    if (err.response.status === 400) {
      res.status = Status.INTERNAL_ERROR;
      res.message = (err.response?.data as unknown as any)?.message ?? null;
      res.data = err.response.data;
      return res;

      // Additional handling for 400 status code
    } else if (err.response.status === 401) {
      console.log('UNAUTHORIZED');
      console.log(err.response)
      res.status = Status.USER_NAME_IS_TAKEN;
      res.data = err?.response?.data;
      res.message = (err.response?.data as unknown as any)?.message ?? null;

      return res;
      // Additional handling for 401 status code
    } else if (err.response.status === 404) {
      res.status = Status.RESOURCE_NOT_FOUND;
      res.data = err?.response?.data;
      res.message = (err.response?.data as unknown as any)?.message ?? null;

      return res;
      // Additional handling for 401 status code
    }
    return res;
  } else if (err.request) {
    // The request was made but no response was received
    console.error('Request made, but no response received');
    console.error('Request config:', err.config);
    return res;
  } else {
    // Something happened in setting up the request that triggered an Error
    console.error('Error setting up the request:', err.message);
    return res;
  }
}

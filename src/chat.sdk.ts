import io, {Socket} from 'socket.io-client';
import {ChatAppSdkOptions, DeleteMessageDto, EventMessage, EventType, Message, ServiceResponse} from './types';
import {AuthService} from './types/services/auth.service';

/**
 * Class representing a WebSocket-based chat application SDK.
 */
export class ChatAppSdk {
  /**
   * The WebSocket connection instance.
   * @type {Socket}
   * @private
   */
  private socket:any;

  public username: string = ''

  private _AuthService: AuthService;
  constructor(baseUrl: string) {
    this._AuthService = new AuthService(baseUrl)
  }

  /**
  * Logs in a user.
  * @param {Object} params - The login parameters.
  * @param {string} params.username - The username of the user.
  * @param {string} params.password - The password of the user.
  * @returns {Promise<ServiceResponse<ChatAppSdkOptions>>} A promise that resolves to the authentication response.
  */
  async login(params: { username: string; password: string }): Promise<ServiceResponse<ChatAppSdkOptions>> {
    const data = await this._AuthService.login(params)

    this.username = `${data.data?.username}`
    return data
  }

  /**
  * Registers a new user.
  * @param {Object} params - The registration parameters.
  * @param {string} params.username - The username of the new user.
  * @param {string} params.password - The password of the new user.
  * @param {string} params.confirmPassword - The confirmation password of the new user.
  * @returns {Promise<ServiceResponse<ChatAppSdkOptions>>} A promise that resolves to the registration response.
  */
  async register(params: { username: string; password: string; confirmPassword: string }): Promise<ServiceResponse<ChatAppSdkOptions>> {
    const data = await this._AuthService.register(params)

    this.username = `${data.data?.username}`
    return data
  }


  /**
   * Connects to the WebSocket server.
   * @param {ChatAppSdkOptions} options - The options for connecting to the WebSocket server.
   */
  async connect(options: ChatAppSdkOptions): Promise<void> {


    if (!options?.token || !options?.username || !options?.wsUrl) {
      console.log(`Please log in first `);
      return;
    }
    this.socket = io(options.wsUrl, {
      query: { token: options.token },
    });

    this.socket.on('connect', () => {
      console.log('Connected to WebSocket server');
    });
  }

  /**
   * Sends a message.
   * @param {EventMessage<string>} dto - The message data.
   */
  sendMessage(dto: EventMessage<string>): void {
    this.socket.emit('message', dto);
  }

  /**
   * Deletes a message.
   * @param {EventMessage<DeleteMessageDto>} dto - The message deletion data.
   */
  deleteMessage(dto: EventMessage<DeleteMessageDto>): void {
    this.socket.emit('deleteMessage',dto);
  }

  /**
   * Sets up a callback for receiving delete message events.
   * @param {(data: EventMessage<Message>) => void} callback - The callback function to handle delete message events.
   */
  onDeleteMessage(callback: (data: EventMessage<Message>) => void): void {


    this.socket.on(EventType.DELETE_MESSAGE_RES, callback);
  }

  /**
   * Sets up a callback for receiving chat messages.
   * @param {(data: EventMessage<Message[]>) => void} callback - The callback function to handle chat message events.
   */
  onGetMessages(callback: (data: EventMessage<Message[]>) => void): void {
    this.socket.on(EventType.GET_CHAT_RES, callback);
  }

    /**
   * Sets up a callback for receiving new message
   * @param {(data: EventMessage<Message>) => void} callback - The callback function to handle chat message events.
   */
    onGetNewMessage(callback: (data: EventMessage<Message>) => void): void {
      this.socket.on(EventType.NEW_MESSAGE, callback);
    }
  /**
   * Disconnects from the WebSocket server.
   */
  disconnect(): void {
    this.socket.disconnect();
  }
}

export function createChatAppSdk(baseUrl: string) {

  return new ChatAppSdk(baseUrl)

}
